#!/bin/bash

opt_setup=false
if [ "$1" = setup ]; then
    opt_setup=true
fi

set -e

src=origin
gitlab_url="https://gitlab.com/xen-project/hardware/xen.git"
dst=xenbits
xenbits_url="/home/xen/git/xen.git"

# Location of local repo
repo_local=$HOME/gitlab-xenbits-mirroring

### testing
# git clone --bare --mirror --reference ~/work/xen https://xenbits.xen.org/git-http/xen.git xenbits
# git clone --bare --mirror --reference ~/work/xen https://gitlab.com/xen-project/hardware/xen.git hw-xen
# gitlab_url="`pwd`/hw-xen"
# xenbits_url="`pwd`/xenbits"

if $opt_setup; then
    set -x
    br="staging"
    git clone --branch "$br" --single-branch --no-tags --bare "$gitlab_url" "$repo_local"
    pushd "$repo_local" >/dev/null

    git config --unset-all "remote.$src.fetch" ||:
    br_list="$(git ls-remote --heads "$src" 'refs/heads/staging*' | cut -f2- | sort --version-sort)"
    for br in $br_list; do
        if ! [[ $br =~ ^refs/heads/staging(-4\.[0-9]+)?$ ]]; then
            echo >&2 "Malformed staging branch: $br"
            exit 1
        fi
        git config --add "remote.$src.fetch" "$br:$br"
    done
    git fetch "$src"

    git remote add --no-tags -t "$br" "$dst" "$xenbits_url"
    git config --unset-all "remote.$dst.fetch"

    popd >/dev/null
fi

pushd "$repo_local" >/dev/null

# Look for new branches not present in the current repo
new_branches="$(comm -13 <(git ls-remote --heads . | cut -f2) <(git ls-remote --heads "$src" 'refs/heads/staging-*' | cut -f2))"
for br in $new_branches; do
    if [[ $br =~ ^refs/heads/(staging-)([0-9]+)\.([0-9]+)$ ]]; then
        # prefix=${BASH_REMATCH[1]}
        # major=${BASH_REMATCH[2]}
        # minor=${BASH_REMATCH[3]}

        # Check that the branch exist in main repo.
        if [ $(git ls-remote --heads "$dst" "$br" | wc -l) -ne 1 ]; then
            echo >&2 "New branch $br found, but doesn't exit in xenbits"
            exit 1
        fi
        ## Potential check for new branch
        # if [ $(git branch -l "$prefix$major.$((minor-1))" | wc -l) -ne 1 ]; then
        #     echo >&2 "New branch $br found, but $prefix$major.$((minor-1)) doesn't exit"
        #     exit 1
        # fi
        echo >&2 "Adding branch $br to mirror"
        git config --add "remote.$src.fetch" "$br:$br"
    else
        echo >&2 "Malformed new staging branch: $br"
        exit 1
    fi
done

git fetch --quiet "$src"
# git push --all --quiet "$dst"
